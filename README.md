# Automação de testes com selenium && cucumber 

### Sobre

- Classe de configuração do cucumber:
com.github.telmagama.testeselenium.RunCucumberTest

#### Versões
- Cucumber 7.0
- Selenium 3.141.59
- JUnit 5
- Java 11

### Como Rodar

- Basta executar os testes com o JUnit no eclipse
