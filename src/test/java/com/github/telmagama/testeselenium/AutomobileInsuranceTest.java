package com.github.telmagama.testeselenium;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.xpath;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.springframework.core.io.ClassPathResource;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

@SuppressWarnings("static-access")
public class AutomobileInsuranceTest {

	private static final String BASE_URL = "http://sampleapp.tricentis.com/101/app.php";

	private WebDriver driver;
	JavascriptExecutor js;
	Actions actions;

	@Before
	public void before() {

		ClassPathResource res = new ClassPathResource("chromedriver");

		File file = new File(res.getPath());
		Set<PosixFilePermission> perms = new HashSet<>();
		perms.add(PosixFilePermission.OWNER_READ);
		perms.add(PosixFilePermission.OWNER_WRITE);
		perms.add(PosixFilePermission.OWNER_EXECUTE);

		perms.add(PosixFilePermission.OTHERS_READ);
		perms.add(PosixFilePermission.OTHERS_WRITE);
		perms.add(PosixFilePermission.OTHERS_EXECUTE);

		perms.add(PosixFilePermission.GROUP_READ);
		perms.add(PosixFilePermission.GROUP_WRITE);
		perms.add(PosixFilePermission.GROUP_EXECUTE);

		try {
			Files.setPosixFilePermissions(file.toPath(), perms);
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.setProperty("webdriver.chrome.driver", file.getPath());
		driver = new ChromeDriver();
		js = (JavascriptExecutor) driver;
		actions = new Actions(driver);
	}

	@After
	public void after() {
		driver.quit();
	}

	@Given("dado aplicação aberta")
	public void dado_aplicação_aberta() {
		driver.get(BASE_URL);
	}

	@When("Preencher fomulario da aba {string}")
	public void preencher_fomulario_da_aba(String string) {
		System.out.println(driver.getTitle());
	}

	@When("Selecionar Make")
	public void selecionar_fabricante() {
		Select make = new Select(driver.findElement(id("make")));
		make.selectByVisibleText("Volkswagen");
	}

	@When("Selecinar Model")
	public void selecinar_model() {
		Select model = new Select(driver.findElement(id("model")));
		model.selectByVisibleText("Three-Wheeler");
	}

	@When("Preencher Cylinder Capacity [ccm] {string}")
	public void preencher_cylinder_capacit(String cylindercapacity) {
		driver.findElement(id("cylindercapacity")).sendKeys(cylindercapacity);
	}

	@When("Preencher Engine Performance [kW] {string}")
	public void preencher_engine_performance_k_w(String engineperformance) {
		driver.findElement(id("engineperformance")).sendKeys(engineperformance);
	}

	@When("Preencher Date of Manufacture {string}")
	public void preencher_date_of_manufacture(String dateofmanufacture) {
		driver.findElement(id("dateofmanufacture")).sendKeys(dateofmanufacture);
	}

	@When("Number of Seats {string}")
	public void number_of_seats(String _numberofseats) {
		Select numberofseats = new Select(driver.findElement(id("numberofseats")));
		numberofseats.selectByVisibleText(_numberofseats);

		WebElement righthanddriveyes = driver.findElement(id("righthanddriveyes"));

		if (righthanddriveyes.isDisplayed()) {
			actions.moveToElement(righthanddriveyes).click().build().perform();
		}
	}

	@When("Number of Seats Motorcycle {string}")
	public void number_of_seats_motorcycle(String _numberofseatsmotorcycle) {
		Select numberofseatsmotorcycle = new Select(driver.findElement(id("numberofseatsmotorcycle")));
		numberofseatsmotorcycle.selectByVisibleText(_numberofseatsmotorcycle);
	}

	@When("Fuel Type {string}")
	public void fuel_type(String _fuel) {
		Select fuel = new Select(driver.findElement(id("fuel")));
		fuel.selectByVisibleText(_fuel);
	}

	@When("Payload [kg] {string}")
	public void payload(String _payload) {
		driver.findElement(id("payload")).sendKeys(_payload);
	}

	@When("Total Weight [kg] {string}")
	public void totalweight(String _totalweight) {
		driver.findElement(id("totalweight")).sendKeys(_totalweight);
	}

	@When("List Price [$] {string}")
	public void list_price_$(String _listprice) {
		driver.findElement(id("listprice")).sendKeys(_listprice);

	}

	@When("License Plate Number {string}")
	public void license_plate_number(String _licenseplatenumber) {
		driver.findElement(id("licenseplatenumber")).sendKeys(_licenseplatenumber);

	}

	@When("Annual Mileage [mi] {string}")
	public void annual_mileage_mi(String _annualmileage) {
		driver.findElement(id("annualmileage")).sendKeys(_annualmileage);

	}

	@When("Click em next para iniciar preenchimento da proxima aba")
	public void click_em_next_para_iniciar_preenchimento_da_proxima_aba() {
		driver.findElement(id("nextenterinsurantdata")).click();
	}

	@When("First Name {string}")
	public void first_name(String _firstname) {
		driver.findElement(id("firstname")).sendKeys(_firstname);

	}

	@When("Last Name {string}")
	public void last_name(String _lastname) {
		driver.findElement(id("lastname")).sendKeys(_lastname);

	}

	@When("Date of Birth {string}")
	public void date_of_birth(String _birthdate) {
		driver.findElement(id("birthdate")).sendKeys(_birthdate);

	}

	@When("Gender")
	public void gender() {
		WebElement element = driver.findElement(id("gendermale").name("Gender"));
		actions.moveToElement(element).click().build().perform();
	}

	@When("Street Address {string}")
	public void street_address(String _streetaddress) {
		driver.findElement(id("streetaddress")).sendKeys(_streetaddress);
	}

	@When("Country {string}")
	public void country(String _country) {
		Select country = new Select(driver.findElement(id("country")));
		country.selectByVisibleText(_country);
	}

	@When("Zip Code {string}")
	public void zip_code(String _zipcode) {
		driver.findElement(id("zipcode")).sendKeys(_zipcode);
	}

	@When("City {string}")
	public void city(String _city) {
		driver.findElement(id("city")).sendKeys(_city);

	}

	@When("Occupation {string}")
	public void occupation(String _occupation) {
		Select occupation = new Select(driver.findElement(id("occupation")));
		occupation.selectByVisibleText(_occupation);
	}

	@When("Hobbies")
	public void hobbies() {
		WebElement element = driver.findElement(id("speeding"));
		actions.moveToElement(element).click().build().perform();
	}

	@When("Website {string}")
	public void website(String _website) {
		driver.findElement(id("website")).sendKeys(_website);
	}

	@When("click next")
	public void next() {
		driver.findElement(id("nextenterproductdata")).click();
	}

	@When("Start Date {string}")
	public void start_date(String _startdate) {
		driver.findElement(id("startdate")).sendKeys(_startdate);
	}

	@When("Insurance Sum [$] {string}")
	public void insurance_sum_$(String _insurancesum) {
		Select insurancesum = new Select(driver.findElement(id("insurancesum")));
		insurancesum.selectByValue(_insurancesum);
	}

	@When("Merit Rating {string}")
	public void merit_rating(String _meritrating) {
		Select meritrating = new Select(driver.findElement(id("meritrating")));
		meritrating.selectByVisibleText(_meritrating);
	}

	@When("Damage Insurance {string}")
	public void damage_insurance(String _damageinsurance) {
		Select damageinsurance = new Select(driver.findElement(id("damageinsurance")));
		damageinsurance.selectByVisibleText(_damageinsurance);
	}

	@When("Optional Products")
	public void optional_products() {
		driver.findElement(cssSelector(".field:nth-child(5) .ideal-radiocheck-label:nth-child(1)")).click();
	}

	@When("Courtesy Car {string}")
	public void courtesy_car(String _courtesycar) {
		Select courtesycar = new Select(driver.findElement(id("courtesycar")));
		courtesycar.selectByVisibleText(_courtesycar);
	}

	@When("Next tab click")
	public void next_tab_click() {
		driver.findElement(id("nextselectpriceoption")).click();
	}

	@When("Select price option")
	public void select_price_option() {
		WebElement selectultimate = driver.findElement(id("selectultimate"));
		actions.moveToElement(selectultimate).click().build().perform();
	}

	@When("Click and go to end tab")
	public void go_to_end_tab() {
		driver.findElement(id("sendquote")).click();
	}

	@When("E-Mail {string}")
	public void e_mail(String _email) {
		driver.findElement(id("email")).sendKeys(_email);
	}

	@When("Phone {string}")
	public void phone(String _phone) {
		driver.findElement(id("phone")).sendKeys(_phone);

	}

	@When("Username {string}")
	public void username(String _username) {
		driver.findElement(id("username")).sendKeys(_username);

	}

	@When("Password {string}")
	public void password(String _password) {
		driver.findElement(id("password")).sendKeys(_password);

	}

	@When("Confirm Password {string}")
	public void confirm_password(String _confirmpassword) {
		driver.findElement(id("confirmpassword")).sendKeys(_confirmpassword);

	}

	@When("Comments")
	public void comments() {
		WebElement textarea = driver.findElement(id("Comments"));

		textarea.sendKeys(Keys.TAB);
		textarea.clear();
		textarea.sendKeys("O seguro ficou caro!");
	}

	@When("Enviar cotacao de seguro")
	public void enviar_cotacao_de_seguro() {
		driver.findElement(id("sendemail")).click();

	}

	@When("Ler confirmcao do sweetalert2 e confirmar sucesso")
	public void ler_confirmcao_do_sweetalert2_e_confirmar_sucesso() {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		WebElement findElement = driver.findElement(xpath("/html/body/div[4]/h2"));

		assertEquals("Sending e-mail success!", findElement.getText());
	}

	@Then("Close browser")
	public void click_em_next_para_iniciar_preenchimento_daa_proxima_aba() {
		System.out.println("Ação movida para o after");
	}
}
