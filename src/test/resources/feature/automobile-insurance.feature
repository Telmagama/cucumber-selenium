Feature: Abrir aplicação no browser e preencher formulario corretamente

   
  Scenario: 02 - Abrir aplicação 'http://sampleapp.tricentis.com/101/app.php' no navegar chrome
  	Given dado aplicação aberta
  	When Preencher fomulario da aba 'Enter Vehicle Data'
  	And Selecionar Make
  	And Selecinar Model
  	And Preencher Cylinder Capacity [ccm] "3"
  	And Preencher Engine Performance [kW] "1.0"
  	And Preencher Date of Manufacture "10/10/1910"
  	And Number of Seats "2"
  	And Number of Seats Motorcycle "2"
  	And Fuel Type "Diesel"
  	And Payload [kg] "1000"
  	And Total Weight [kg] "4000"
  	And List Price [$] "3555"
  	And License Plate Number "60000"
  	And Annual Mileage [mi] "1000"
  	And Click em next para iniciar preenchimento da proxima aba 
  	#aba 02
  	And First Name "Carla"
	 	And Last Name "Santos"
	 	And Date of Birth "10/10/1990"
	 	And Gender
	 	And Street Address "1000"
	 	And Country "Anguilla"
	 	And Zip Code "3400"
	 	And City "Goiania"
	 	And Occupation "Employee"
	 	And Hobbies
	 	And Website "conta.com.br"
	 	And click next
	 	#tab 3
	 	And Start Date "09/01/2022"
	 	And Insurance Sum [$] "3000000"
	 	And Merit Rating "Bonus 2"
	 	And Damage Insurance "No Coverage"
	 	And Optional Products
	 	And Courtesy Car "No"
	 	And Next tab click
	 	#tab 4
	 	And Select price option
	 	And Click and go to end tab
	 	#tab 5
	 	And E-Mail "Mari@gmail.com"
	 	And Phone "559788595"
	 	And Username "Mari.com"
	 	And Password "Muda123#"
	 	And Confirm Password "Muda123#"
	 	And Comments
	 	And Enviar cotacao de seguro
	 	#end
	 	And Ler confirmcao do sweetalert2 e confirmar sucesso
  	Then Close browser  
  	
# 	Scenario: 03 - Enter Vehicle Data
#	 	Given Dado formulario preenchido na aba Enter Vehicle Data
#	 	When Preencher formulario da aba Enter Insurant Data
#	 	And First Name
#	 	And Last Name
#	 	And Date of Birth
#	 	And Gender
#	 	And Street Address
#	 	And Country
#	 	And Zip Code
#	 	And City
#	 	And Occupation
#	 	And Hobbies
#	 	And Website
#	 	Then Click em next para iniciar preenchimento da proxima aba  
 
  	